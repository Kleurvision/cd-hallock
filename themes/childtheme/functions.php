<?php 
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
	
	function theme_enqueue_styles() {
		wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
		
		// Google Forms Override
		wp_enqueue_style( 'gforms-style', get_stylesheet_directory_uri() . '/css/gforms.css' );

		// Fonts
		wp_enqueue_style( 'fonts-style', get_stylesheet_directory_uri() . '/css/font-style.css' );
		
		// Buttons
		wp_enqueue_style( 'button-style', get_stylesheet_directory_uri() . '/css/buttons.css' );
		
	}
?>