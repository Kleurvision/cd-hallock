<?php 
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
	
	function theme_enqueue_styles() {
		wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
		
		// Google Forms Override
		wp_enqueue_style( 'gforms-style', get_stylesheet_directory_uri() . '/css/gforms.css' );

		// Fonts
		wp_enqueue_style( 'fonts-style', get_stylesheet_directory_uri() . '/css/font-style.css' );
		
		// Buttons
		wp_enqueue_style( 'button-style', get_stylesheet_directory_uri() . '/css/buttons.css' );
        
        // Custom Styles
        wp_enqueue_style( 'custom-styles', get_stylesheet_directory_uri() . '/css/custom_style.css' );

        //Scripts
        wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/js/scripts.js', array( 'jquery' ) );
		
	}
	
function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyD5IMJAB4op21OyL57HPMuv5iiTpFeTq1U');
}

add_action('acf/init', 'my_acf_init');

if ( ! function_exists('location_post_type') ) {

// Register Custom Post Type
function location_post_type() {

	$labels = array(
		'name'                  => _x( 'Locations', 'Post Type General Name', 'childtheme' ),
		'singular_name'         => _x( 'Location', 'Post Type Singular Name', 'childtheme' ),
		'menu_name'             => __( 'Locations', 'childtheme' ),
		'name_admin_bar'        => __( 'Locations', 'childtheme' ),
		'archives'              => __( 'Item Archives', 'childtheme' ),
		'attributes'            => __( 'Item Attributes', 'childtheme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'childtheme' ),
		'all_items'             => __( 'All Items', 'childtheme' ),
		'add_new_item'          => __( 'Add New Item', 'childtheme' ),
		'add_new'               => __( 'Add New', 'childtheme' ),
		'new_item'              => __( 'New Item', 'childtheme' ),
		'edit_item'             => __( 'Edit Item', 'childtheme' ),
		'update_item'           => __( 'Update Item', 'childtheme' ),
		'view_item'             => __( 'View Item', 'childtheme' ),
		'view_items'            => __( 'View Items', 'childtheme' ),
		'search_items'          => __( 'Search Item', 'childtheme' ),
		'not_found'             => __( 'Not found', 'childtheme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'childtheme' ),
		'featured_image'        => __( 'Featured Image', 'childtheme' ),
		'set_featured_image'    => __( 'Set featured image', 'childtheme' ),
		'remove_featured_image' => __( 'Remove featured image', 'childtheme' ),
		'use_featured_image'    => __( 'Use as featured image', 'childtheme' ),
		'insert_into_item'      => __( 'Insert into item', 'childtheme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'childtheme' ),
		'items_list'            => __( 'Items list', 'childtheme' ),
		'items_list_navigation' => __( 'Items list navigation', 'childtheme' ),
		'filter_items_list'     => __( 'Filter items list', 'childtheme' ),
	);
	$args = array(
		'label'                 => __( 'Location', 'childtheme' ),
		'description'           => __( 'Locations', 'childtheme' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-location-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'location', $args );

}
add_action( 'init', 'location_post_type', 0 );

}

/* Shortcode Example for calling menu */
function menu_func( $atts ){
	// override default attributes with user attributes
     return wp_nav_menu(array('menu' => 'Main Menu', 'echo' => false, 'menu_class'=> 'footer-menu-1',));
}
add_shortcode( 'cdhallock_menu', 'menu_func' );

/* Shortcode for location title */
function menu_func2( $atts ){
	// override default attributes with user attributes
    $the_title = "<h1>" . get_the_title() . "</h1>";
    return $the_title;
}
add_shortcode( 'location_title', 'menu_func2' );
