<?php

/* TEMPLATE NAME: Locations */

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>


<style type="text/css">

.acf-map {
	width: 100%;
	height: 100vh;
	
	border: #ccc solid 1px;
	margin: 00px 0 20px;
}

/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5IMJAB4op21OyL57HPMuv5iiTpFeTq1U"></script>
<script type="text/javascript">
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	// vars
	var args = {
		zoom		: 12,
		center		: new google.maps.LatLng(-27.6648, 81.5158),
		mapTypeId	: google.maps.MapTypeId.ROADMAP,
	    scrollwheel: false,
	    mapTypeControl: false,
	    streetViewControl: false,


		styles : [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	
	var radius =  $marker.attr('data-radius');

	var iconURL = '<?php echo get_stylesheet_directory_uri(); ?>/images/map-pin.png';
	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
        icon        : iconURL,
		map			: map
        
	});
	
		// Add circle overlay and bind to marker
	var circle = new google.maps.Circle({
	  map: map,
	  radius: radius * 1609.34,    // 10 miles in metres
	  fillColor: '#a7a9ac',
	  strokeWeight: 1
	});
	circle.bindTo('center', marker, 'position');

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>


<div id="main-content">
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="entry-content">
			<?php
				the_content();
			?>
		</div> <!-- .entry-content -->
	<?php endwhile; ?>



	<?php
	$loop = new WP_Query( array(
	    'post_type' => 'Location',
	    'posts_per_page' => -1
	  )
	);
    
    
	?>
	
	<div class="acf-map">
		<?php while ( $loop->have_posts() ) : $loop->the_post();
			
				 $location = get_field('address');
				 $radius   = get_field('radius');
				 
				?>
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" data-radius="<?php echo $radius; ?>">
				    <div class="row map-marker-wrap">
                        <div class="col-sm-4 map-pin-image">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full', array('class' => 'location-image')); ?></a>
                        </div>
                        <div class="col-sm-8 map-pin-info">
                            <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                            <p class="address"><?php echo $location['address']; ?></p>
                        </div>
                    </div>
					<!--<p><?php //the_content(); ?></p>-->
				</div>	
		<?php endwhile; wp_reset_query(); ?>
	</div>


</div> <!-- #main-content -->

<?php
echo do_shortcode('[et_pb_section global_module="455"][/et_pb_section]');
get_footer();
