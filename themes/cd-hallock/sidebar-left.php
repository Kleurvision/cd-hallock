<div id="sidebar-left" class="col-sm-12 col-md-4 col-lg-4 sidebar-left" role="complementary">
    <div class="location-featured-image">
        <?php the_post_thumbnail('full'); ?>
    </div>
    <div class="contact-info">
        <h3 class="contact-title">Contact</h3>
        <hr>
        <div class="contact-details">
            <p><?php the_field('contact_info'); ?></p>
        </div>
    </div>          
</div>