
<?php get_header(); ?>
<div class="container-fluid">
<div id="content" class="clearfix row single-location-wrapper">

    <div id="main" class="col-sm-12 clearfix" role="main">
        
        <?php 
        
        if (have_posts()) : while (have_posts()) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix location-info'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <?php
                echo do_shortcode('[et_pb_section global_module="520"][/et_pb_section]');
            ?>
            <section class="location_content clearfix container-fluid" itemprop="articleBody">
                    <div class="row location-row">
                       <?php get_sidebar('left'); // sidebar 1 ?>
                        <div class="col-sm-12 col-md-8 col-lg-8 clearfix location-inner-content">
                            <div class="about-facility">
                                <h2>About This Facility</h2>
                                <?php
                                    the_content();
                                ?>
                            </div>
                            <div class="location-services">
                                <h3>Services Available</h3>
                                <div class="row service-row">    
                                <?php
                                    $numOfCols = 2;
                                    $rowCount = 0;
                                    $bootstrapColWidth = 12 / $numOfCols;
                                    $service_count = 1;
                                    // check if the repeater field has rows of data
                                    if( have_rows('services_available') ):

                                        // loop through the rows of data
                                        while ( have_rows('services_available') ) : the_row(); 
                                    
                                        switch ($service_count) {
                                            case 1:
                                                $service_image = 'red-circle.png';
                                                break;
                                            case 2:
                                                $service_image = 'orange-circle.png';
                                                break;
                                            case 3:
                                                $service_image = 'yellow-circle.png';
                                                break;
                                            case 4:
                                                $service_image = 'purple-circle.png';
                                                break;
                                            case 5:
                                                $service_image = 'green-circle.png';
                                                break;
                                            case 6:
                                                $service_image = 'teal-circle.png';
                                                break;
                                            case 7:
                                                $service_image = 'blue-circle.png';
                                                break;
                                        }
                                    ?>
                                        
                                        <div class="service-block col-md-<?php echo $bootstrapColWidth; ?>">
                                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/<?php echo $service_image; ?>" /> 
                                            <div class="service-details">
                                                <h4 class="service-title">
                                                    <?php
                                                        // display a service title
                                                        the_sub_field('service_title');
                                                    ?>
                                                </h4>
                                                <p class="service-description">
                                                    <?php
                                                        // display a service description
                                                        the_sub_field('service_description');
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                        
                                        <?php
                                            if ($service_count <= 7) {
                                                $service_count++;
                                            } else {
                                                $service_count = 1;
                                            }
                                            $rowCount++;
                                            if($rowCount % $numOfCols == 0) echo '</div><div class="row service-row">';
                                        endwhile;

                                    else :

                                        // no rows found

                                    endif;

                                ?>
                                </div>
                            </div>
                            <div class="location-contact">
                                <div class="row contact-details">
                                    <div class="col-md-2 contact-photo">
                                        <img src="<?php the_field('contact_profile_picture'); ?>" />
                                    </div>
                                    <div class="col-md-10 contact-name">
                                        <h5>Main Contact</h5>
                                        <p><?php the_field('contact_name'); ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 contact-bio">
                                        <p><?php the_field('contact_bio'); ?>"</p>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
            </section> <!-- end article section -->
        </article> <!-- end article -->

        <?php endwhile; ?>

        <?php else : ?>

        <article id="post-not-found">
            <header>
                <h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
            </header>
            <section class="post_content">
                <p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
            </section>
            <footer>
            </footer>
        </article>

        <?php endif; ?>

    </div> <!-- end #main -->

</div> <!-- end #content -->
</div>
<?php 
echo do_shortcode('[et_pb_section global_module="455"][/et_pb_section]');
get_footer(); ?>