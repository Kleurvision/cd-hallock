jQuery(document).ready(function ($) {
   var maxHeight = -1;

   $(".job-listings .job-listing").each(function() {
     maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
   });

   $(".job-listings .job-listing").each(function() {
     $(this).height(maxHeight);
   });
});